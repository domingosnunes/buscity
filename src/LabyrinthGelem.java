//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//



import java.awt.Color;
import java.awt.Graphics;
import pt.ua.gboard.Gelem;

public class LabyrinthGelem extends Gelem {
    public static final int ROAD_NW = 1;
    public static final int ROAD_N = 2;
    public static final int ROAD_NE = 4;
    public static final int ROAD_W = 8;
    public static final int ROAD_E = 16;
    public static final int ROAD_SW = 32;
    public static final int ROAD_S = 64;
    public static final int ROAD_SE = 128;
    public static final int ROAD = 256;
    protected static double gapPercSize = 20.0D;
    protected static boolean rounded = true;
    protected static boolean showRoadBoundaries = false;
    protected static Color roadColor;
    protected static Color wallColor;
    protected static Color roadBoundariesColor;
    protected final int form;

    public static void setGapPercentageSize(double var0) {
        assert var0 >= 0.0D && var0 <= 100.0D : "invalid gap percentage";

        gapPercSize = var0;
    }

    public static void setShowRoadBoundaries() {
        showRoadBoundaries = true;
    }

    public static void resetShowRoadBoundaries() {
        showRoadBoundaries = false;
    }

    public static boolean validForm(int var0) {
        return var0 >= 0 && var0 <= 511;
    }

    public LabyrinthGelem(int var1) {
        this(var1, 1, 1);
    }

    public LabyrinthGelem(int var1, int var2, int var3) {
        super(var2, var3);

        assert validForm(var1);

        this.form = var1;
    }

    public boolean isWall() {
        return (this.form & 256) == 0;
    }

    public boolean isRoad() {
        return !this.isWall();
    }

    protected void drawErase(Graphics var1, int var2, int var3, int var4, int var5, Color var6, boolean var7) {
        if (var7) {
            int var8 = var4 * this.numberOfColumns;
            int var9 = var5 * this.numberOfLines;
            int var10 = (int)((double)var8 * gapPercSize) / 100;
            int var11 = (int)((double)var9 * gapPercSize) / 100;
            int var12 = var8 - 2 * var10;
            int var13 = var9 - 2 * var11;
            var1.setColor(roadColor);
            var1.fillRect(var3 * var4, var2 * var5, var8, var9);
            if (this.isWall()) {
                var1.setColor(wallColor);
                if (rounded) {
                    var1.fillRoundRect(var3 * var4 + var10, var2 * var5 + var11, var12, var13, var12, var13);
                    if ((this.form & 2) == 0) {
                        var1.fillRect(var3 * var4 + var10, var2 * var5 + var11, var12, var13 / 2 + var13 % 2);
                    }

                    if ((this.form & 16) == 0) {
                        var1.fillRect(var3 * var4 + var10 + var12 / 2, var2 * var5 + var11, var12 / 2 + var12 % 2, var13);
                    }

                    if ((this.form & 64) == 0) {
                        var1.fillRect(var3 * var4 + var10, var2 * var5 + var11 + var13 / 2, var12, var13 / 2 + var13 % 2);
                    }

                    if ((this.form & 8) == 0) {
                        var1.fillRect(var3 * var4 + var10, var2 * var5 + var11, var12 / 2 + var12 % 2, var13);
                    }

                    if ((this.form & 1) != 0 && (this.form & 2) == 0 && (this.form & 8) == 0) {
                        var1.fillRect(var3 * var4, var2 * var5, var10, var11);
                        var1.setColor(roadColor);
                        var1.fillRoundRect(var3 * var4 - var10, var2 * var5 - var11, var10 * 2, var11 * 2, var10 * 2, var11 * 2);
                        var1.setColor(wallColor);
                    }

                    if ((this.form & 4) != 0 && (this.form & 2) == 0 && (this.form & 16) == 0) {
                        var1.fillRect(var3 * var4 + var10 + var12, var2 * var5, var10, var11);
                        var1.setColor(roadColor);
                        var1.fillRoundRect(var3 * var4 + var10 + var12, var2 * var5 - var11, var10 * 2, var11 * 2, var10 * 2, var11 * 2);
                        var1.setColor(wallColor);
                    }

                    if ((this.form & 32) != 0 && (this.form & 64) == 0 && (this.form & 8) == 0) {
                        var1.fillRect(var3 * var4, var2 * var5 + var11 + var13, var10, var11);
                        var1.setColor(roadColor);
                        var1.fillRoundRect(var3 * var4 - var10, var2 * var5 + var11 + var13, var10 * 2, var11 * 2, var10 * 2, var11 * 2);
                        var1.setColor(wallColor);
                    }

                    if ((this.form & 128) != 0 && (this.form & 64) == 0 && (this.form & 16) == 0) {
                        var1.fillRect(var3 * var4 + var10 + var12, var2 * var5 + var11 + var13, var10, var11);
                        var1.setColor(roadColor);
                        var1.fillRoundRect(var3 * var4 + var10 + var12, var2 * var5 + var11 + var13, var10 * 2, var11 * 2, var10 * 2, var11 * 2);
                        var1.setColor(wallColor);
                    }
                } else {
                    var1.fillRect(var3 * var4 + var10, var2 * var5 + var11, var12, var13);
                }

                if ((this.form & 2) == 0) {
                    var1.fillRect(var3 * var4 + var10, var2 * var5, var12, var11);
                }

                if ((this.form & 16) == 0) {
                    var1.fillRect(var3 * var4 + var10 + var12, var2 * var5 + var11, var10, var13);
                }

                if ((this.form & 64) == 0) {
                    var1.fillRect(var3 * var4 + var10, var2 * var5 + var11 + var13, var12, var11);
                }

                if ((this.form & 8) == 0) {
                    var1.fillRect(var3 * var4, var2 * var5 + var11, var10, var13);
                }

                if ((this.form & 2) == 0 && (this.form & 8) == 0 && (this.form & 1) == 0) {
                    var1.fillRect(var3 * var4, var2 * var5, var10, var11);
                }

                if ((this.form & 2) == 0 && (this.form & 16) == 0 && (this.form & 4) == 0) {
                    var1.fillRect(var3 * var4 + var10 + var12, var2 * var5, var10, var11);
                }

                if ((this.form & 64) == 0 && (this.form & 8) == 0 && (this.form & 32) == 0) {
                    var1.fillRect(var3 * var4, var2 * var5 + var11 + var13, var10, var11);
                }

                if ((this.form & 64) == 0 && (this.form & 16) == 0 && (this.form & 128) == 0) {
                    var1.fillRect(var3 * var4 + var10 + var12, var2 * var5 + var11 + var13, var10, var11);
                }
            } else if (showRoadBoundaries) {
                var1.setColor(roadBoundariesColor);
                var1.drawRect(var3 * var4, var2 * var5, var8 - 1, var9 - 1);
            }
        } else {
            var1.setColor(var6);
            var1.fillRect(var3 * var4, var2 * var5, var4 * this.numberOfColumns(), var5 * this.numberOfLines());
        }

    }

    static {
        roadColor = Color.darkGray;
        wallColor = Color.red;
        roadBoundariesColor = Color.yellow;
    }
}
