import java.util.ArrayList;

public class Route {

    private String name;
    private Character[] route;
    private ArrayList<BusStop> stoplist;

    public Route(String name, Character [] route){
        this.name = name;
        this.route = route;
    }

    public void setStoplist(ArrayList<BusStop> stoplist) {
        this.stoplist = stoplist;
    }

    public String getName() {
        return name;
    }

    public Character[] getRoute() {
        return route;
    }

    public ArrayList<BusStop> getStoplist() {
        return stoplist;
    }
}
