//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import pt.ua.gboard.GBoard;
import pt.ua.gboard.Gelem;
import pt.ua.gboard.basic.Position;

public class Labyrinth {
    protected static int numberOfLayers = 2;
    protected static String windowName = "Labyrinth";
    public final int numberOfLines;
    public final int numberOfColumns;
    public final GBoard board;
    protected boolean outsideIsRoad;
    protected int gelemCellsSize;
    protected final char[] roadSymbols;
    protected final char[] wallSymbols;
    protected final Gelem[] roadSymbolGelems;
    protected final Gelem[] wallSymbolGelems;
    protected Position[] roadSymbolsPositions;
    protected int[][] map;
    protected static LabyrinthGelem[] gelems = null;
    protected static final int UNDEFINED = -1;
    protected static final int OUTSIDE = 0;
    protected static final int ROAD = 65536;
    protected static final int WALL = 131072;

    public static synchronized int numberOfLayers() {
        return numberOfLayers;
    }

    public static synchronized void setNumberOfLayers(int var0) {
        assert var0 >= 1 : "invalid number of layers: " + var0;

        numberOfLayers = var0;
    }

    public static boolean validMapFile(String var0) {
        boolean var1 = var0 != null;
        if (var1) {
            File var2 = new File(var0);
            var1 = var2.exists() && !var2.isDirectory() && var2.canRead();
        }

        return var1;
    }

    public static synchronized String windowName() {
        return windowName;
    }

    public static synchronized void setWindowName(String var0) {
        assert var0 != null : "invalid window name";

        windowName = var0;
    }

    public Labyrinth(String var1) {
        this((String[])loadMap(var1), (char[])null, 1);
    }

    public Labyrinth(String var1, char[] var2) {
        this((String[])loadMap(var1), var2, 1);
    }

    public Labyrinth(String var1, int var2) {
        this((String[])loadMap(var1), (char[])null, var2);
    }

    public Labyrinth(String var1, char[] var2, int var3) {
        this(loadMap(var1), var2, var3);
    }

    public Labyrinth(String var1, char[] var2, int var3, boolean var4) {
        this(loadMap(var1), var2, var3, var4);
    }

    public Labyrinth(String[] var1) {
        this((String[])var1, (char[])null, 1);
    }

    public Labyrinth(String[] var1, char[] var2) {
        this((String[])var1, var2, 1);
    }

    public Labyrinth(String[] var1, int var2) {
        this((String[])var1, (char[])null, var2);
    }

    public Labyrinth(String[] var1, char[] var2, int var3) {
        this(var1, var2, var3, false);
    }

    public Labyrinth(String[] var1, char[] var2, int var3, boolean var4) {
        assert var1 != null && var1.length > 0 : "invalid maze (or file not read)";

        assert var3 > 0 : "invalid gelem cell size: " + var3;

        this.outsideIsRoad = var4;
        if (var2 != null) {
            this.roadSymbols = new char[var2.length + 1];
            System.arraycopy(var2, 0, this.roadSymbols, 0, var2.length);
            this.roadSymbols[var2.length] = ' ';
            this.roadSymbolGelems = new Gelem[var2.length + 1];
        } else {
            this.roadSymbols = null;
            this.roadSymbolGelems = new Gelem[1];
        }

        char[] var5 = new char[0];

        int var6;
        int var7;
        for(var6 = 0; var6 < var1.length; ++var6) {
            for(var7 = 0; var7 < var1[var6].length(); ++var7) {
                char var8 = var1[var6].charAt(var7);
                if (!symbolExists(var8, this.roadSymbols) && !symbolExists(var8, var5)) {
                    char[] var9 = new char[var5.length + 1];
                    System.arraycopy(var5, 0, var9, 0, var5.length);
                    var9[var5.length] = var8;
                    var5 = var9;
                }
            }
        }

        this.wallSymbols = var5;
        this.wallSymbolGelems = new Gelem[var5.length];
        this.roadSymbolsPositions = new Position[0];
        this.gelemCellsSize = var3;
        this.numberOfLines = var1.length;
        var6 = var1[0].length();

        for(var7 = 1; var7 < var1.length; ++var7) {
            if (var6 < var1[var7].length()) {
                var6 = var1[var7].length();
            }
        }

        this.numberOfColumns = var6;
        this.map = new int[this.numberOfLines][this.numberOfColumns];

        int var10;
        for(var7 = 0; var7 < this.numberOfLines; ++var7) {
            for(var10 = 0; var10 < this.numberOfColumns; ++var10) {
                this.map[var7][var10] = -1;
            }
        }

        for(var7 = 0; var7 < this.numberOfLines; ++var7) {
            this.fillOutside(var1, var7, 0);
            this.fillOutside(var1, var7, this.numberOfColumns - 1);
        }

        for(var7 = 0; var7 < this.numberOfColumns; ++var7) {
            this.fillOutside(var1, 0, var7);
            this.fillOutside(var1, this.numberOfLines - 1, var7);
        }

        for(var7 = 0; var7 < this.numberOfLines; ++var7) {
            for(var10 = 0; var10 < var1[var7].length(); ++var10) {
                if (!this.isOutside(var7, var10)) {
                    char var11 = var1[var7].charAt(var10);
                    if (this.isRoadSymbol(var11)) {
                        this.map[var7][var10] = 65536 | var11;
                        if (var11 != ' ') {
                            this.addRoadSymbolPosition(var7, var10);
                        }
                    } else {
                        this.map[var7][var10] = 131072 | var11;
                    }
                }
            }
        }

        assert this.mapDefined();

        this.board = new GBoard(windowName, this.numberOfLines * var3, this.numberOfColumns * var3, 25 / var3, 25 / var3, numberOfLayers);
        this.createGelems();

        for(var7 = 0; var7 < this.numberOfLines; ++var7) {
            for(var10 = 0; var10 < this.numberOfColumns; ++var10) {
                this.board.draw(this.fetchGelem(var7, var10), var7 * var3, var10 * var3, 0);
            }
        }

    }

    public boolean validPosition(int var1, int var2) {
        return var1 >= 0 && var1 < this.numberOfLines && var2 >= 0 && var2 < this.numberOfColumns;
    }

    public boolean isOutside(int var1, int var2) {
        assert this.validPosition(var1, var2) : "invalid position: (" + var1 + "," + var2 + ")";

        return this.map[var1][var2] == 0;
    }

    public boolean isRoadSymbol(char var1) {
        boolean var2 = var1 == ' ';
        if (!var2 && this.roadSymbols != null) {
            var2 = symbolExists(var1, this.roadSymbols);
        }

        return var2;
    }

    public boolean isWallSymbol(char var1) {
        return !this.isRoadSymbol(var1);
    }

    public boolean isRoad(int var1, int var2) {
        assert this.validPosition(var1, var2) : "invalid position: (" + var1 + "," + var2 + ")";

        return (this.map[var1][var2] & 16711680) == 65536;
    }

    public boolean isWall(int var1, int var2) {
        assert this.validPosition(var1, var2) : "invalid position: (" + var1 + "," + var2 + ")";

        return (this.map[var1][var2] & 16711680) == 131072;
    }

    public char roadSymbol(int var1, int var2) {
        assert this.isRoad(var1, var2) : "position (" + var1 + "," + var2 + ") is not a road";

        return (char)(this.map[var1][var2] & '\uffff');
    }

    public char wallSymbol(int var1, int var2) {
        assert this.isWall(var1, var2) : "position (" + var1 + "," + var2 + ") is not a wall";

        return (char)(this.map[var1][var2] & '\uffff');
    }

    public Position[] roadSymbolPositions(char var1) {
        assert this.isRoadSymbol(var1) && var1 != ' ' : "char '" + var1 + "' is not an external road symbol";

        int var3 = 0;

        int var4;
        for(var4 = 0; var4 < this.roadSymbolsPositions.length; ++var4) {
            if (this.roadSymbol(this.roadSymbolsPositions[var4].line(), this.roadSymbolsPositions[var4].column()) == var1) {
                ++var3;
            }
        }

        Position[] var2 = new Position[var3];
        var3 = 0;

        for(var4 = 0; var4 < this.roadSymbolsPositions.length; ++var4) {
            if (this.roadSymbol(this.roadSymbolsPositions[var4].line(), this.roadSymbolsPositions[var4].column()) == var1) {
                var2[var3++] = this.roadSymbolsPositions[var4];
            }
        }

        return var2;
    }

    public Position[] symbolPositions(char var1) {
        int var3 = 0;

        int var4;
        int var5;
        for(var4 = 0; var4 < this.numberOfLines; ++var4) {
            for(var5 = 0; var5 < this.numberOfColumns; ++var5) {
                if ((char)(this.map[var4][var5] & '\uffff') == var1) {
                    ++var3;
                }
            }
        }

        Position[] var2 = new Position[var3];
        var4 = 0;

        for(var5 = 0; var4 < var3 && var5 < this.numberOfLines; ++var5) {
            for(int var6 = 0; var4 < var3 && var6 < this.numberOfColumns; ++var6) {
                if ((char)(this.map[var5][var6] & '\uffff') == var1) {
                    var2[var4++] = new Position(var5, var6);
                }
            }
        }

        return var2;
    }

    public void putRoadSymbol(int var1, int var2, char var3) {
        assert this.isRoad(var1, var2) : "position (" + var1 + "," + var2 + ") is not a road";

        assert this.isRoadSymbol(var3) : "char '" + var3 + "' is not a road symbol";

        if (this.isRoadSymbolAttachToGelem(this.roadSymbol(var1, var2))) {
            this.eraseGelem(this.gelemAttachedToRoadSymbol(this.roadSymbol(var1, var2)), var1, var2);
        }

        if (this.existsRoadSymbolPosition(var1, var2)) {
            this.removeRoadSymbolPosition(var1, var2);
        }

        this.map[var1][var2] = this.map[var1][var2] & 16711680 | var3;
        if (this.isRoadSymbolAttachToGelem(var3)) {
            this.drawGelem(this.gelemAttachedToRoadSymbol(var3), var1, var2);
        }

        if (var3 != ' ') {
            this.addRoadSymbolPosition(var1, var2);
        }

    }

    public void putWallSymbol(int var1, int var2, char var3) {
        assert this.isWall(var1, var2) : "position (" + var1 + "," + var2 + ") is not a wall";

        assert this.isWallSymbol(var3) : "char '" + var3 + "' is not a wall symbol";

        this.map[var1][var2] = this.map[var1][var2] & 16711680 | var3;
    }

    public boolean isRoadSymbolAttachToGelem(char var1) {
        assert this.isRoadSymbol(var1) : "char '" + var1 + "' is not a road symbol";

        int var2 = indexOfSymbol(var1, this.roadSymbols);
        return this.roadSymbolGelems[var2] != null;
    }

    public Gelem gelemAttachedToRoadSymbol(char var1) {
        assert this.isRoadSymbol(var1) : "char '" + var1 + "' is not a road symbol";

        assert this.isRoadSymbolAttachToGelem(var1) : "char '" + var1 + "' is not attached to a gelem";

        int var2 = indexOfSymbol(var1, this.roadSymbols);
        return this.roadSymbolGelems[var2];
    }

    public void attachGelemToRoadSymbol(char var1, Gelem var2) {
        assert this.isRoadSymbol(var1) : "char '" + var1 + "' is not a road symbol";

        assert var2 != null : "null gelem!";

        assert !this.isRoadSymbolAttachToGelem(var1) : "char '" + var1 + "' is already attached to a gelem";

        int var3 = indexOfSymbol(var1, this.roadSymbols);
        this.roadSymbolGelems[var3] = var2;
        this.drawAllRoadGelem(var1, var2);
    }

    public void detachGelemToRoadSymbol(char var1, Gelem var2) {
        assert this.isRoadSymbol(var1) : "char '" + var1 + "' is not a road symbol";

        assert this.isRoadSymbolAttachToGelem(var1) : "char '" + var1 + "' is not attached to a gelem";

        int var3 = indexOfSymbol(var1, this.roadSymbols);
        this.eraseAllRoadGelem(var1, this.roadSymbolGelems[var3]);
        this.roadSymbolGelems[var3] = null;
    }

    public boolean isWallSymbolAttachToGelem(char var1) {
        assert this.isWallSymbol(var1) : "char '" + var1 + "' is not a wall symbol";

        int var2 = indexOfSymbol(var1, this.wallSymbols);
        return this.wallSymbolGelems[var2] != null;
    }

    public Gelem gelemAttachedToWallSymbol(char var1) {
        assert this.isWallSymbol(var1) : "char '" + var1 + "' is not a wall symbol";

        assert this.isWallSymbolAttachToGelem(var1) : "char '" + var1 + "' is not attached to a gelem";

        int var2 = indexOfSymbol(var1, this.wallSymbols);
        return this.wallSymbolGelems[var2];
    }

    public void attachGelemToWallSymbol(char var1, Gelem var2) {
        assert this.isWallSymbol(var1) : "char '" + var1 + "' is not a wall symbol";

        assert var2 != null : "null gelem!";

        assert !this.isWallSymbolAttachToGelem(var1) : "char '" + var1 + "' is already attached to a gelem";

        int var3 = indexOfSymbol(var1, this.wallSymbols);
        this.wallSymbolGelems[var3] = var2;
        this.drawAllWallGelem(var1, var2);
    }

    public void detachGelemToWallSymbol(char var1, Gelem var2) {
        assert this.isWallSymbol(var1) : "char '" + var1 + "' is not a wall symbol";

        assert this.isWallSymbolAttachToGelem(var1) : "char '" + var1 + "' is not attached to a gelem";

        int var3 = indexOfSymbol(var1, this.wallSymbols);
        this.eraseAllWallGelem(var1, this.wallSymbolGelems[var3]);
        this.wallSymbolGelems[var3] = null;
    }

    protected static boolean symbolExists(char var0, char[] var1) {
        assert var1 != null;

        return indexOfSymbol(var0, var1) < var1.length;
    }

    protected static int indexOfSymbol(char var0, char[] var1) {
        assert var1 != null;

        int var2;
        for(var2 = 0; var2 < var1.length && var1[var2] != var0; ++var2) {
            ;
        }

        return var2;
    }

    protected void drawGelem(Gelem var1, int var2, int var3) {
        this.board.draw(var1, var2 * this.gelemCellsSize, var3 * this.gelemCellsSize, 1);
    }

    protected void eraseGelem(Gelem var1, int var2, int var3) {
        this.board.erase(var1, var2 * this.gelemCellsSize, var3 * this.gelemCellsSize, 1);
    }

    protected void drawAllRoadGelem(char var1, Gelem var2) {
        for(int var3 = 0; var3 < this.numberOfLines; ++var3) {
            for(int var4 = 0; var4 < this.numberOfColumns; ++var4) {
                if (this.isRoad(var3, var4) && this.roadSymbol(var3, var4) == var1) {
                    this.drawGelem(var2, var3, var4);
                }
            }
        }

    }

    protected void eraseAllRoadGelem(char var1, Gelem var2) {
        for(int var3 = 0; var3 < this.numberOfLines; ++var3) {
            for(int var4 = 0; var4 < this.numberOfColumns; ++var4) {
                if (this.isRoad(var3, var4) && this.roadSymbol(var3, var4) == var1) {
                    this.eraseGelem(var2, var3, var4);
                }
            }
        }

    }

    protected void drawAllWallGelem(char var1, Gelem var2) {
        for(int var3 = 0; var3 < this.numberOfLines; ++var3) {
            for(int var4 = 0; var4 < this.numberOfColumns; ++var4) {
                if (this.isWall(var3, var4) && this.wallSymbol(var3, var4) == var1) {
                    this.drawGelem(var2, var3, var4);
                }
            }
        }

    }

    protected void eraseAllWallGelem(char var1, Gelem var2) {
        for(int var3 = 0; var3 < this.numberOfLines; ++var3) {
            for(int var4 = 0; var4 < this.numberOfColumns; ++var4) {
                if (this.isWall(var3, var4) && this.wallSymbol(var3, var4) == var1) {
                    this.eraseGelem(var2, var3, var4);
                }
            }
        }

    }

    protected boolean isRoadUnbounded(int var1, int var2) {
        boolean var3 = !this.validPosition(var1, var2);
        if (this.validPosition(var1, var2)) {
            var3 = this.isRoad(var1, var2) || this.isOutside(var1, var2);
        }

        return var3;
    }

    protected void createGelems() {
        if (gelems == null) {
            gelems = new LabyrinthGelem[512];

            for(int var1 = 0; var1 < 512; ++var1) {
                gelems[var1] = new LabyrinthGelem(var1, this.gelemCellsSize, this.gelemCellsSize);
            }
        }

    }

    protected Gelem fetchGelem(int var1, int var2) {
        int var3 = 0;
        if (this.isRoadUnbounded(var1, var2)) {
            var3 |= 256;
        }

        if (this.isRoadUnbounded(var1 - 1, var2 - 1)) {
            var3 |= 1;
        }

        if (this.isRoadUnbounded(var1 - 1, var2)) {
            var3 |= 2;
        }

        if (this.isRoadUnbounded(var1 - 1, var2 + 1)) {
            var3 |= 4;
        }

        if (this.isRoadUnbounded(var1, var2 - 1)) {
            var3 |= 8;
        }

        if (this.isRoadUnbounded(var1, var2 + 1)) {
            var3 |= 16;
        }

        if (this.isRoadUnbounded(var1 + 1, var2 - 1)) {
            var3 |= 32;
        }

        if (this.isRoadUnbounded(var1 + 1, var2)) {
            var3 |= 64;
        }

        if (this.isRoadUnbounded(var1 + 1, var2 + 1)) {
            var3 |= 128;
        }

        return gelems[var3];
    }

    protected static String[] loadMap(String var0) {
        assert validMapFile(var0) : "Path \"" + var0 + "\" is not valid";

        String[] var1 = null;

        try {
            File var2 = new File(var0);
            Scanner var3 = new Scanner(var2);
            String[] var4 = new String[(int)Math.sqrt((double)var2.length())];

            int var5;
            for(var5 = 0; var3.hasNextLine(); ++var5) {
                if (var5 == var4.length) {
                    String[] var6 = new String[var4.length + 10];
                    System.arraycopy(var4, 0, var6, 0, var4.length);
                    var4 = var6;
                }

                var4[var5] = var3.nextLine();
            }

            var3.close();
            var1 = new String[var5];

            for(int var8 = 0; var8 < var5; ++var8) {
                var1[var8] = var4[var8];
            }
        } catch (IOException var7) {
            var1 = null;
        }

        return var1;
    }

    protected void fillOutside(String[] var1, int var2, int var3) {
        if (this.validPosition(var2, var3) && this.map[var2][var3] == -1 && (var3 >= var1[var2].length() || this.isRoadSymbol(var1[var2].charAt(var3)))) {
            this.map[var2][var3] = this.outsideIsRoad ? 65536 : 0;
            this.fillOutside(var1, var2 + 1, var3 + 0);
            this.fillOutside(var1, var2 - 1, var3 + 0);
            this.fillOutside(var1, var2 + 0, var3 + 1);
            this.fillOutside(var1, var2 + 0, var3 - 1);
        }

    }

    protected boolean mapDefined() {
        boolean var1 = true;

        for(int var2 = 0; var1 && var2 < this.numberOfLines; ++var2) {
            for(int var3 = 0; var1 && var3 < this.numberOfColumns; ++var3) {
                var1 = this.map[var2][var3] != -1;
            }
        }

        return var1;
    }

    protected boolean existsRoadSymbolPosition(int var1, int var2) {
        assert this.isRoad(var1, var2);

        boolean var3 = false;

        for(int var4 = 0; !var3 && var4 < this.roadSymbolsPositions.length; ++var4) {
            var3 = this.roadSymbolsPositions[var4].line() == var1 && this.roadSymbolsPositions[var4].column() == var2;
        }

        return var3;
    }

    protected void addRoadSymbolPosition(int var1, int var2) {
        assert !this.existsRoadSymbolPosition(var1, var2);

        Position[] var3 = new Position[this.roadSymbolsPositions.length + 1];
        System.arraycopy(this.roadSymbolsPositions, 0, var3, 0, this.roadSymbolsPositions.length);
        var3[this.roadSymbolsPositions.length] = new Position(var1, var2);
        this.roadSymbolsPositions = var3;
    }

    protected void removeRoadSymbolPosition(int var1, int var2) {
        assert this.existsRoadSymbolPosition(var1, var2);

        Position[] var3 = new Position[this.roadSymbolsPositions.length - 1];

        int var4;
        for(var4 = 0; var4 < this.roadSymbolsPositions.length && (this.roadSymbolsPositions[var4].line() != var1 || this.roadSymbolsPositions[var4].column() != var2); ++var4) {
            ;
        }

        System.arraycopy(this.roadSymbolsPositions, 0, var3, 0, var4);
        System.arraycopy(this.roadSymbolsPositions, var4 + 1, var3, var4, this.roadSymbolsPositions.length - var4 - 1);
        this.roadSymbolsPositions = var3;
    }
}
