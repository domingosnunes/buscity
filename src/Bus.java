
import pt.ua.concurrent.Mutex;
import pt.ua.gboard.GBoard;
import pt.ua.gboard.Gelem;
import pt.ua.gboard.basic.*;
import static java.lang.System.*;

import java.awt.*;
import java.util.ArrayList;

public class Bus {
    private ComposedGelem drawBus;
    private StringGelem drawIndex;
    final private GBoard board;
    final private Labyrinth city;
    private int l;
    private int c;
    private Direction direction;
    private int passengers = 0;
    private int newL;
    private int newC;
    protected int busNumber;
    private int rightL, rightC, frontL, frontC, leftL, leftC;
    protected boolean doorsOpen;
    protected BusStop stop = null;
    private Supervisor supervisor;
    private boolean started = false;
    protected boolean stopEmpty = true;
    protected int wait2Leave;
    private Route route;
    protected ArrayList<Civilian> civilians;

    public Bus(Supervisor supervisor, Labyrinth city, GBoard board, int busNumber) {
        this.supervisor = supervisor;
        this.board = board;
        this.city = city;
        this.busNumber = busNumber;

    }

    //Movimento de uma casa segundo a sua orientação atual
    public void move() {
        assert started : "Bus has not started";

        newL = l;
        newC = c;

        switch (direction) {
            case E: {

                // Next Position
                newC++;

                break;
            }
            case W:

                // Next Position
                newC--;

                break;
            case N:

                // Next Position
                newL--;

                break;
            case S:

                // Next Position
                newL++;

                break;
        }


        if (!board.validPosition(newL, newC) || !board.gelemFitsInside(drawBus, newL, newC)) {
            out.println("Invalid Position");
        }else if(city.isWall(newL, newC)){
            turnAround();
        }
        else {
            board.move(drawBus, l, c, newL, newC);

            l = newL;
            c = newC;
        }

        updateTurnCoordinates();
    }

    //Verifica se na posição atual existe uma paragem, intersepção e que chegou ao final da rota
    public String checkRoad() {
        assert started : "Bus has not started";

        String result = "Clear";

        if (city.isWall(rightL, rightC)) {
            if ('b' == city.wallSymbol(rightL, rightC)) {
                result = "Bus Stop";
            }
        }
        if (city.isRoad(rightL, rightC) || city.isRoad(leftL,leftC)) {
            result = "Interception";
        }
        if (city.roadSymbol(l,c) == 'F'){
            result = "Bye";
        }

        return result;
    }
    
    public void turnRight(){
        assert started : "Bus has not started";


        switch (direction) {
            case E:
                direction = Direction.S;
                break;
            case W:
                direction = Direction.N;
                break;
            case N:
                direction = Direction.E;
                break;
            case S:
                direction = Direction.W;
                break;
        }
        drawBusGelem();


    }

    public void turnAround(){
        assert started : "Bus has not started";


        switch (direction) {
            case E:
                direction = Direction.W;
                break;
            case W:
                direction = Direction.E;
                break;
            case N:
                direction = Direction.S;
                break;
            case S:
                direction = Direction.N;
                break;
        }
        drawBusGelem();


    }

    public void turnLeft(){
        assert started : "Bus has not started";


        switch (direction) {
            case E:
                direction = Direction.N;
                break;
            case W:
                direction = Direction.S;
                break;
            case N:
                direction = Direction.W;
                break;
            case S:
                direction = Direction.E;
                break;
        }
        drawBusGelem();

    }

    //Encontra a paragem de autocarro da posição actual e o numero de passageiros que pretendem entrar ou sair
    public void busStop(SharedBus b) {

        stop = supervisor.getBusStop(rightL,rightC).result();

        assert stop != null : "Stop is null";

        for(Civilian c : civilians){
            if(c.busStopWork == stop) wait2Leave++;
        }

        if(stop.busCheckIn(b) > 0 ) stopEmpty = false;
    }


    public void openDoors(){
        doorsOpen = true;
    }

    //Entrada de um passageiro, fazendo update na contagem
    public void enterBus(Civilian c){
        assert doorsOpen : "Doors not open";

        civilians.add(c);

        passengers++;
        stopEmpty = stop.updateStop();
    }


    public void want2Leave(BusStop s){
        assert stop == s : "Stop is different";
    }

    //Saida do autocarro, fazendo update da contagem
    public void exitBus(Civilian c){
        assert doorsOpen : "Doors not open";

        civilians.remove(c);

        passengers--;
        wait2Leave--;
    }

    public boolean hasAllExited(){
        assert wait2Leave >= 0 : "Wait2Leave is negative" ;
        return wait2Leave == 0;
    }

    public void closedDoors(){

        stop = null;
        doorsOpen = false;
        stopEmpty = true;

    }

    public Position getPos(){
        assert started : "Bus has not started";

        return new Position(l,c);
    }

    public Position getRight(){
        assert started : "Bus has not started";

        return new Position(rightL,rightC);
    }

    public void setRoute(Route route){
        assert route != null : "Route is null";

        this.route = route;
    }

    public String getRoute(){
        assert route != null : "Route is null";
        return route.getName();
    }

    //Desenha a imagem de autocarro respeitando a sua orientação
    private void drawBusGelem() {

        board.erase(drawBus, l, c);

        ImageGelem drawBusImg;

        switch (direction) {
            case E:
                drawBusImg = new ImageGelem("busE.png", board, 80);
                break;
            case N:
                drawBusImg = new ImageGelem("busN.png", board, 80);
                break;
            case S:
                drawBusImg = new ImageGelem("busS.png", board, 80);
                break;
            case W:
                drawBusImg = new ImageGelem("busW.png", board, 80);
                break;
            default:
                drawBusImg = new ImageGelem("busW.png", board, 80);
                break;
        }

        Gelem[] comp = {drawBusImg, drawIndex};
        drawBus = new ComposedGelem(comp);
        board.draw(drawBus, l, c, 1);


    }

    //Atualiza as coordenadas em seu redor
    private void updateTurnCoordinates(){

        switch (direction) {
            case E:
                rightL = l + 1;
                rightC = c;
                frontL = l;
                frontC = c + 1;
                leftL = l - 1;
                leftC = c;
                break;
            case N:
                rightL = l;
                rightC = c + 1;
                frontL = l - 1;
                frontC = c;
                leftL = l;
                leftC = c - 1;
                break;
            case S:
                rightL = l;
                rightC = c - 1;
                frontL = l + 1;
                frontC = c;
                leftL = l;
                leftC = c + 1;
                break;
            case W:
                rightL = l - 1;
                rightC = c;
                frontL = l;
                frontC = c - 1;
                leftL = l + 1;
                leftC = c;
                break;
        }
    }

    public void eraseGelem(){
        board.erase(drawBus, l, c, 1);
    }


    public void reset() {
        doorsOpen = false;
        wait2Leave = 0;

        Position[] position = city.roadSymbolPositions('T');
        l = position[0].line();
        c = position[0].column();
        direction = Direction.E;

        civilians = new ArrayList<>();

        drawIndex = new StringGelem(Integer.toString(busNumber), Color.white, 40);
        ImageGelem drawBusImg = new ImageGelem("busE.png", board, 80);
        Gelem[] comp = {drawBusImg, drawIndex};
        drawBus = new ComposedGelem(comp);
        board.draw(drawBus, l, c, 1);

        started  = true;
    }
}
