import pt.ua.concurrent.Future;
import pt.ua.gboard.GBoard;
import pt.ua.gboard.basic.CircleGelem;
import pt.ua.gboard.basic.Position;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;

import static java.lang.System.out;

public class Civilian extends Person {
    private static int index = 1;
    private int id;
    private Supervisor supervisor;
    protected BusStop busStopHome;
    protected BusStop busStopWork;

    public Civilian(Supervisor supervisor, Labyrinth city, GBoard board) {
        super(supervisor,city,board,Color.blue);
        this.id = index++;
        this.supervisor = supervisor;

        setDaemon(true);

        start();
    }

    @Override
    public void arun() {
        super.arun();
        out.println("Civilian "+ id + " started");

        //Encontra o caminho da posição inicial até à paragem mais proxima
        ArrayList<Position> pathHome = findClosestStop(getHome());

        //Encontra o caminho desde da paragem de destino e o seu destino
        ArrayList<Position> pathWork = findClosestStop(getWork());

        Collections.reverse(pathWork);

        String route = getBestRoute(pathHome.get(pathHome.size() - 1),pathWork.get(0));

        Position homeStop = pathHome.get(pathHome.size() - 1);
        Position exitStop = pathWork.get(0);

        //Identifica as paragens de entrada e saida
        busStopHome = supervisor.getBusStop(homeStop.line(), homeStop.column()).result();
        busStopWork = supervisor.getBusStop(exitStop.line(), exitStop.column()).result();

        if(!route.equals("None")){
            go(pathHome);

            SharedBus bus = busStopHome.civilianEnterStop(route);

            bus.enterBus(this);

            erasePerson();

            bus.want2Leave(busStopWork);

            bus.exitBus(this);

            drawPerson(pathWork.get(0));

            go(pathWork);
        }

        erasePerson();



    }


    public int getIndex() {
        return id;
    }
}
