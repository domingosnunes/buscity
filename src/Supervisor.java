import pt.ua.concurrent.*;
import pt.ua.gboard.GBoard;
import pt.ua.gboard.basic.Position;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.*;

import static java.lang.System.out;

public class Supervisor extends Actor {

    private static HashMap<SharedBus, Boolean> busList;
    private static ArrayList<BusStop> stopsList;
    private static ArrayList<Route> routesList;

    private final Labyrinth city;
    private final GBoard board;

    public Supervisor(Labyrinth city, GBoard board){
        super();
        assert city != null: "Null reference to City";
        assert board != null: "Null reference to Board";

        this.city = city;
        this.board = board;

        busList = new HashMap<>();
        stopsList = new ArrayList<>();
        routesList = new ArrayList<>();

        addAllBusStop();

        start();
    }

    //Encontra todas as paragens do mapa
    private void addAllBusStop(){

        for(int x = 0; x < city.numberOfLines; x++){
            for (int y = 0; y < city.numberOfColumns; y++) {
                if(city.isWall(x,y)) {
                    if (city.wallSymbol(x, y) == 'b') {
                        stopsList.add(new BusStop(city,board,x,y));
                    }
                }
            }
        }

    }

    //Lançamento do Ghost Driver
    public void exploreRoute(Route route) {

        GhostDriver g = new GhostDriver(this,city,board,route);

        try {
            g.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        route.setStoplist(g.getStopList());

        routesList.add(route);
    }

    //Cria a quantidade de autocarros pretendida
    public Future createFleat(int numBuses) {
        assert numBuses > 0 : "Number of Buses is low";
        final Supervisor supervisor = this;
        final Future result = new Future(false);
        Routine r = new Routine()
        {
            final Supervisor s = supervisor;
            final Future future = result;
            public void execute()
            {
                for(int i = 0; i < numBuses; i++){
                    busList.put(new SharedBus(s,city,board,i),false);
                }

                futureDone(future);
            }
        };
        inPendingRoutine(r);
        return result;
    }

    //Dando a paragem inicial e final do trajecto que prentende fazer, determina qual é a melhor rota e devolve o nome da rota ou None se não encontrar
    public Future<String> getRoute(BusStop ini, BusStop end){
        assert ini != null: "Stop null";
        assert end != null: "Stop null";

        final Future<String> result = new Future(true);
        Routine r = new Routine()
        {
            final Future<String> future = result;
            public void execute()
            {
                future.setResult("None");

                for(Route r : routesList){
                    ArrayList<BusStop> list = r.getStoplist();

                    if(list.contains(ini) && list.contains(end)){
                        if(list.indexOf(ini) < list.indexOf(end)){
                            future.setResult(r.getName());
                        }
                    }
                }

                futureDone(future);
            }
        };
        inPendingRoutine(r);
        return result;
    }

    //Devolve a paragem de autocarro segundo as coordenadas
    public Future<BusStop> getBusStop(int l, int c) {
        assert l > 0 && c > 0: "Invalid positions";

        final Future<BusStop> result = new Future<>(true);
        Routine r = new Routine()
        {
            final int line = l;
            final int collumn = c;
            final Future<BusStop> future = result;
            public void execute()
            {
                for(BusStop stop : stopsList){
                    if(stop.getPosL() == line && stop.getPosC() == collumn) {
                        future.setResult(stop);
                        break;
                    }
                }

                futureDone(future);
            }
        };
        inPendingRoutine(r);
        return result;
    }

    //O driver acabou o seu trajecto lança esta função para colocar o autocarro que usou como disponível.
    public Future finishedRoute(SharedBus bus) {
        assert bus != null: "Invalid positions";

        final Future result = new Future<>(true);
        Routine r = new Routine()
        {

            final Future future = result;
            public void execute()
            {
                busList.replace(bus,false);

                futureDone(future);
            }
        };
        inPendingRoutine(r);
        return result;
    }

    //Inicia o Driver e associa um autocarro que esteja disponivel, se possível.
    public Future startDriver(Driver driver) {
        assert driver != null: "Invalid positions";

        final Future result = new Future<>(true);
        Routine r = new Routine()
        {
            final Future future = result;
            public void execute()
            {
                for (Map.Entry<SharedBus, Boolean> entry : busList.entrySet()) {
                    SharedBus bus = entry.getKey();
                    Boolean bool = entry.getValue();
                    if (!bool) {
                        bus.reset();
                        driver.setBus(bus);
                        busList.replace(bus,true);
                        break;
                    }
                }

                driver.start();
                futureDone(future);
            }
        };
        inPendingRoutine(r);
        return result;
    }

}
