import pt.ua.concurrent.CThread;
import pt.ua.gboard.basic.CircleGelem;
import pt.ua.gboard.basic.Position;
import pt.ua.gboard.*;
import pt.ua.gboard.shapes.Circle;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.TreeMap;

import static java.lang.System.*;

public class Person extends CThread {

    private Position home;
    private Position work;
    private GBoard board;
    private Supervisor supervisor;
    private Labyrinth city;
    private Position pos;
    private Random random;
    private Color color;
    private CircleGelem drawPerson;
    private CircleGelem drawPerson1;

    public Person(Supervisor supervisor, Labyrinth city, GBoard board, Color color){
        this.board = board;
        this.supervisor = supervisor;
        this.city = city;
        this.color = color;

        random = new Random();

        do{
            home = new Position(random.nextInt(city.numberOfLines), random.nextInt(city.numberOfColumns));
            work = new Position(random.nextInt(city.numberOfLines), random.nextInt(city.numberOfColumns));

        }while (!isSideWalk(home) || !isSideWalk(work));

        pos = home;

        drawPerson = new CircleGelem(color,40.0,true);
        board.draw(drawPerson, getHome().line(), getHome().column(), 1);
    }

    @Override
    public void arun() {
        super.arun();
    }

    //Apaga o desnho da pessoa
    public void erasePerson(){
        board.erase(drawPerson,pos.line(),pos.column());
    }

    //Desenha a pessoa
    public void drawPerson(Position p){
        board.draw(drawPerson,p.line(),p.column(),1);
        pos = p;
    }

    //Muda a cor da pessoa
    public void changeColor(Color color){
        board.erase(drawPerson,pos.line(),pos.column());
        drawPerson = new CircleGelem(color,40.0,true);
        board.draw(drawPerson,pos.line(),pos.column(),1);
    }

    //Encontra a paragem mais perto desde de uma posição
    public ArrayList<Position> findClosestStop(Position position){
        ArrayList<Position> nextHops = getNearSideWalk(position);
        Position actual = nextHops.get(0);
        Position prev = position;
        ArrayList<Position> path1 = new ArrayList<>();
        ArrayList<Position> path2 = new ArrayList<>();
        BusStop stop;

        stop = supervisor.getBusStop(actual.line(),actual.column()).result();

        if(stop != null){
            path1.add(actual);
            return path1;
        }
        while (stop == null){
            nextHops = getNearSideWalk(actual);
            for(Position p : nextHops){
                if(!p.isEqual(prev)) {
                    path1.add(p);
                    prev = actual;
                    actual = p;
                    break;
                }
            }

            stop = supervisor.getBusStop(actual.line(),actual.column()).result();
        }

        nextHops = getNearSideWalk(position);
        actual = nextHops.get(1);
        prev = position;
        stop = supervisor.getBusStop(actual.line(),actual.column()).result();

        if(stop != null){
            path2.add(actual);
            return path1;
        }

        while (stop == null){
            nextHops = getNearSideWalk(actual);

            for(Position p : nextHops){

                if(!p.isEqual(prev)) {
                    path2.add(p);
                    prev = actual;
                    actual = p;
                    break;
                }
            }

            stop = supervisor.getBusStop(actual.line(),actual.column()).result();
        }



        return path1.size() > path2.size() ? path2 : path1;
    }

    //deslocação da pessoa dando um array de posições
    public void go(ArrayList<Position> path){
        for(Position p : path){
            board.move(drawPerson, pos.line(), pos.column(), p.line(), p.column());
            pos = p;


            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    //Encontra o melhor caminho segundo entre duas paragens de autocarro, dando as suas posições como argumentos
    public String getBestRoute(Position inicial, Position end){
        BusStop i = supervisor.getBusStop(inicial.line(),inicial.column()).result();
        BusStop e = supervisor.getBusStop(end.line(),end.column()).result();

        return supervisor.getRoute(i,e).result();
    }

    //Percebe se está no passeio
    private boolean isSideWalk(Position pos){
        if(city.isWall(pos.line(),pos.column())){
            Character c = city.wallSymbol(pos.line(),pos.column());
            if(c == 'p' || c == 'b')  return true;
        }
        return false;
    }

    //Encontra as coordenadas onde se encontra o passeio em seu redor
    public ArrayList<Position> getNearSideWalk(Position pos){
        ArrayList<Position> nextHops = new ArrayList<>();

        if(isSideWalk(new Position(pos.line()+1,pos.column()))) nextHops.add(new Position(pos.line()+1,pos.column()));
        if(isSideWalk(new Position(pos.line()-1,pos.column()))) nextHops.add(new Position(pos.line()-1,pos.column()));
        if(isSideWalk(new Position(pos.line(),pos.column()+1))) nextHops.add(new Position(pos.line(),pos.column()+1));
        if(isSideWalk(new Position(pos.line(),pos.column()-1))) nextHops.add(new Position(pos.line(),pos.column()-1));


        return nextHops;
    }

    public Position getPos() {
        return pos;
    }

    public Position getHome() {
        return home;
    }

    public Position getWork() {
        return work;
    }

    public GBoard getBoard() {
        return board;
    }

}
