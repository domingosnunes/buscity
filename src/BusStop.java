import pt.ua.concurrent.Actor;
import pt.ua.concurrent.Mutex;
import pt.ua.concurrent.MutexCV;
import pt.ua.gboard.GBoard;
import pt.ua.gboard.basic.Position;

import static java.lang.System.*;

import java.util.ArrayList;
import java.util.HashMap;

public class BusStop{
    private final Labyrinth city;
    private final GBoard board;
    private final int posL;
    private final int posC;
    private Mutex mtx;
    private MutexCV noBus;
    private MutexCV notEmptyStop;
    protected MutexCV busHere;
    protected boolean isBusHere;
    protected int peopleWaiting;
    protected int want2Enter;
    protected String busRoute = "null";
    private SharedBus bus;
    private HashMap<String, Integer> waitRoute;

    public BusStop(Labyrinth city, GBoard board,int posL, int posC){
        this.city = city;
        this.board = board;
        this.posL = posL;
        this.posC = posC;

        bus = null;
        isBusHere = false;
        peopleWaiting = 0;
        mtx = new Mutex();
        noBus = mtx.newCV();
        notEmptyStop = mtx.newCV();
        busHere = mtx.newCV();

        waitRoute = new HashMap<>();
    }

    // Os passageiros entram e esperam que o autocarro que está a fazer a rota que eles pretendem chegue
    public SharedBus civilianEnterStop(String route){
        mtx.lock();
        try{
            if(waitRoute.containsKey(route)) {
                int num = waitRoute.get(route);
                waitRoute.replace(route,num+1);
            }else {
                waitRoute.put(route,1);
            }

            while(!isBusHere || !busRoute.equals(route)) noBus.await();

        }
        finally {
            mtx.unlock();
        }

        return bus;
    }

    //O Bus comunica que chegou, alertando os passageiros
    public int busCheckIn(SharedBus b){
        int result;
        mtx.lock();
        try{
            while(isBusHere) busHere.await();
            result = waitRoute.getOrDefault(b.getRoute(), 0);


            if(result > 0){
                bus = b;
                isBusHere = true;
                busRoute = b.getRoute();
            }
            noBus.broadcast();
        }
        finally {
            mtx.unlock();
        }
        return result;
    }

    //Faz update dos passageiros que ainda pretendem entrar
    public boolean updateStop(){
        boolean result;
        mtx.lock();

        try{

            int num = waitRoute.get(bus.getRoute());

            waitRoute.replace(bus.getRoute(),num-1);

            result = waitRoute.get(bus.getRoute()) == 0;

            if(result) {
                isBusHere = false;
                bus = null;
                busHere.broadcast();
            }
        }
        finally {
            mtx.unlock();
        }
        return result;
    }

    public int getPosL() {
        return posL;
    }

    public int getPosC() {
        return posC;
    }
}
