import pt.ua.gboard.GBoard;
import pt.ua.gboard.basic.Position;

import java.awt.*;
import java.util.ArrayList;
import static java.lang.System.*;

public class GhostDriver extends Person {
    protected Bus bus;
    protected Route route;
    protected int interceptions;
    protected ArrayList<BusStop> stopList;
    protected Supervisor supervisor;
    protected Character[] turns;

    public GhostDriver(Supervisor supervisor, Labyrinth city, GBoard board, Route route) {
        super(supervisor, city, board, Color.black);
        this.route = route;
        this.supervisor = supervisor;

        bus = new Bus(supervisor,city,board,99);

        stopList = new ArrayList<>();
        turns = route.getRoute();

        bus.reset();
        start();
    }


    @Override
    public void arun() {
        erasePerson();

        interceptions = 0;
        while(true) {

            String road = bus.checkRoad();

            if(react(road)) break;

            bus.move();

        }

        bus.eraseGelem();
    }

    private boolean react(String road) {
        boolean isFinished = false;

        switch (road){
            case "Bus Stop": {
                Position pos = bus.getRight();


                BusStop stop = supervisor.getBusStop(pos.line(),pos.column()).result();


                stopList.add(stop);
                break;
            }
            case "Interception": {

                char direction = turns[interceptions];


                switch (direction){
                    case 'R' : bus.turnRight(); break;
                    case 'L' : bus.turnLeft(); break;
                    default: break;
                }
                interceptions++;
                break;
            }
            case "Bye": {
                isFinished = true;
                break;
            }
            default: break;
        }

        return isFinished;
    }

    public ArrayList<BusStop> getStopList(){
        return stopList;
    }
}
