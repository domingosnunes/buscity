import pt.ua.concurrent.CThread;
import pt.ua.concurrent.Future;
import pt.ua.gboard.GBoard;
import pt.ua.gboard.basic.CircleGelem;
import pt.ua.gboard.basic.Position;
import pt.ua.gboard.basic.StringGelem;

import static java.lang.System.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.*;

public class Driver extends Person {
    private static int index = 1;
    private int id;
    private SharedBus bus;
    private final Route route;
    private int interceptions;
    private Supervisor supervisor;
    private Character[] turns;

    public Driver(Supervisor supervisor, Labyrinth city, GBoard board, Route route) {
        super(supervisor,city,board,Color.green);
        bus = null;
        this.id = index++;
        this.route = route;
        this.supervisor = supervisor;
        turns = route.getRoute();

        erasePerson();
    }

    @Override
    public void arun() {

        if(bus == null){
            out.println("No bus. No work. Bye");
            return;
        }


        out.println("Driver "+ id + ": started");

        //Indica ao autocarro qual é a rota que vai fazer
        bus.setRoute(route);

        interceptions = 0;

        while(true) {

            String road = bus.checkRoad();

            if(react(road)) break;

            bus.move();


            try {
                sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

        supervisor.finishedRoute(bus).done();

        bus.eraseGelem();
    }

    public void setBus(SharedBus bus){
        this.bus = bus;
    }

    private boolean react(String road) {
        boolean isFinished = false;

        switch (road){
            case "Bus Stop": {
                bus.busStop();

                bus.openDoors();
                bus.closedDoors();


                break;
            }
            case "Interception": {
                char direction = turns[interceptions];


                switch (direction){
                    case 'R' : bus.turnRight(); break;
                    case 'L' : bus.turnLeft(); break;
                    default: break;
                }
                interceptions++;
                break;
            }
            case "Bye": {
                isFinished = true;
                break;
            }
            default: break;
        }

        return isFinished;
    }


    public int getIndex() {
        return id;
    }
}
