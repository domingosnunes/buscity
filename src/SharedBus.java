import pt.ua.concurrent.Mutex;
import pt.ua.concurrent.MutexCV;
import pt.ua.gboard.*;
import pt.ua.gboard.basic.Position;
import static java.lang.System.*;
import pt.ua.gboard.basic.StringGelem;

public class SharedBus{

    protected Bus bus;
    protected Mutex mtx;
    protected MutexCV doorsopen;
    protected MutexCV stopNull;
    protected MutexCV doorsclosed;
    protected MutexCV peopleEntering;
    protected MutexCV wait4Stop;
    protected int peopleOnDoor;

    public SharedBus(Supervisor supervisor, Labyrinth city, GBoard board, int busNumber){
        bus = new Bus(supervisor,city,board,busNumber);

        mtx = new Mutex();
        doorsopen = mtx.newCV();
        stopNull = mtx.newCV();
        doorsclosed = mtx.newCV();
        peopleEntering = mtx.newCV();
        wait4Stop = mtx.newCV();
    }
    
    //Movimento
    public void move(){
        mtx.lock();

        try{
            while (bus.doorsOpen) doorsopen.await();

            bus.move();
        }
        finally {
            mtx.unlock();
        }
    }

    //Verificação do mapa
    public String checkRoad() {
        mtx.lock();
        String result;
        try {
            while (bus.doorsOpen) doorsopen.await();

            result = bus.checkRoad();
        }
        finally {
            mtx.unlock();
        }
        return result;
    }


    public void turnRight() {
        mtx.lock();

        try{
            while (bus.doorsOpen) doorsopen.await();

            bus.turnRight();
        }
        finally {
            mtx.unlock();
        }

    }


    public void turnLeft() {
        mtx.lock();

        try{
            while (bus.doorsOpen) doorsopen.await();

            bus.turnLeft();
        }
        finally {
            mtx.unlock();
        }

    }

    //Invocada quando encontra uma paragem, alerta os passageiros
    public void busStop() {

        mtx.lock();

        try{
            bus.busStop(this);
            wait4Stop.broadcast();
        } finally {
            mtx.unlock();
        }
        out.println("Bus " + bus.busNumber + ": Bus Stop Located");
    }

    //Abre as portas, alerta as funções que estão em espera devido a elas estarem fechadas
    public void openDoors(){
        mtx.lock();

        try{
            bus.openDoors();

            doorsclosed.broadcast();
        }
        finally {
            out.println("Bus " + bus.busNumber + ": Doors open");
            mtx.unlock();
        }
    }

    //Entra no autocarro se as portas estiverem abertas, sinaliza para fechar as portas
    public void enterBus(Civilian c) {
        mtx.lock();

        try {

            while (!bus.doorsOpen) doorsclosed.await();

            bus.enterBus(c);

            peopleEntering.broadcast();

        } finally {
            out.println("Bus " + bus.busNumber + ": Passenger enters");
            mtx.unlock();
        }
    }

    //Espera se a paragem de saida não for a pretendida, sinaliza os passageiros que pretendem ir embora
    public void want2Leave(BusStop s){
        mtx.lock();

        try{

            while (bus.stop != s) wait4Stop.await();

            bus.want2Leave(s);

            doorsclosed.broadcast();

        }
        finally {
            mtx.unlock();
        }
    }

    //Sai do autocarro se as portas estiverem abertas
    public void exitBus(Civilian c){
        mtx.lock();

        try{

            while (!bus.doorsOpen) doorsclosed.await();

            bus.exitBus(c);

            peopleEntering.broadcast();

        }
        finally {
            out.println("Bus " + bus.busNumber + ": Passenger exits");
            mtx.unlock();
        }
    }

    //Fecha as portas se todas as pessoas que pretendem entrar ou sair nesta paragem tenham realizado a sua função
    public void closedDoors(){
        mtx.lock();

        try{
            while (!bus.stopEmpty || !bus.hasAllExited()) {
                peopleEntering.await();
            }

            bus.closedDoors();
            doorsopen.broadcast();
        }
        finally {
            out.println("Bus " + bus.busNumber + ": Doors closed");
            mtx.unlock();
        }
    }


    public Position getPos() {
        return bus.getPos();
    }


    public void setRoute(Route route) {
        bus.setRoute(route);
    }

    public String getRoute(){
        return bus.getRoute();
    }

    public void reset() {
        bus.reset();
    }

    public void eraseGelem() {
        bus.eraseGelem();
    }
}
