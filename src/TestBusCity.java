import pt.ua.gboard.*;
import pt.ua.gboard.basic.*;
import pt.ua.gboard.games.*;
import pt.ua.gboard.shapes.Circle;
import pt.ua.gboard.shapes.ShapeGelem;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

import static java.lang.System.*;


public class TestBusCity {

    static Labyrinth city = null;
    static final char startSymbol = 'S';
    static final char endSymbol = 'F';
    static final char busStopSymbol = 'b';
    static final char walkSymbol = 'p';
    static final char terminalSymbol = 'T';
    static final char crossSymbol = 'c';

    public static void main(String[] args) {

        String map="map.txt";

        if (args.length != 1)
        {
            err.println("Usage: java -ea LabyrinthSolver <text-map>");
            out.println();
            out.println("Using \""+map+"\" as default");
        }
        else
            map=args[0];

        Labyrinth.setWindowName("BusCity");

        char[] extraSymbols =
                {
                        startSymbol,
                        endSymbol,
                        terminalSymbol,
                        crossSymbol
                };



        Gelem[] gelems = {
                new StringGelem(""+startSymbol, Color.red),
                new StringGelem(""+endSymbol, Color.red),
                new StringGelem("Terminal",Color.white,1,7),
                new RectangleGelem(Color.white,50.0,true),
        };



        city = new Labyrinth(map, extraSymbols, 1, true);
        GBoard board = city.board;


        for(int i = 0; i < extraSymbols.length; i++) city.attachGelemToRoadSymbol(extraSymbols[i], gelems[i]);

        Gelem[] busStop = {new FilledGelem(Color.white,100),new ImageGelem("busStop.png",board,100)};

        city.attachGelemToWallSymbol(busStopSymbol,new ComposedGelem(busStop));

        city.attachGelemToWallSymbol(walkSymbol,new RectangleGelem(Color.white,100));


        Position[] startPositions = city.roadSymbolPositions(terminalSymbol);
        if (startPositions.length != 1)
        {
            err.println("ERROR: one, and only one, start point required!");
            exit(2);
        }
        Position[] endPositions = city.roadSymbolPositions(endSymbol);
        if (endPositions.length != 1)
        {
            err.println("ERROR: one, and only one, end point required!");
            exit(3);
        }

        //Criação do Supervisor
        Supervisor supervisor = new Supervisor(city,board);

        //Cria todos os autocarros
        supervisor.createFleat(2).done();

        //As curvas de de cada intersecção
        Character[] route1 = {'R','L','F','F','F','R','R','F','F','L','L','F'};
        Character[] route2 = {'F','R','R','F','F','L','L','F','F','F','R','L'};

        //As rotas associadas com o nome
        Route r1 = new Route("Yellow", route1);
        Route r2 = new Route("Red", route2);

        //Lança o ghost driver e devolve as paragens de autocarro que encontra
        supervisor.exploreRoute(r1);
        supervisor.exploreRoute(r2);

        //Criação os Civis
        ArrayList<Civilian> civilianList = new ArrayList<>();

        for(int i = 0; i < 200; i++)  civilianList.add(new Civilian(supervisor, city, board));

        //4 Drivers
        Driver d1 = new Driver(supervisor, city, board,r1);
        Driver d2 = new Driver(supervisor, city, board,r2);
        Driver d3 = new Driver(supervisor, city, board,r1);
        Driver d4 = new Driver(supervisor, city, board,r2);


        // Espera e lança os dois primeiros drivers
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        supervisor.startDriver(d1).done();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        supervisor.startDriver(d2).done();


        try {
            d1.join();
            d2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Espera que os dois primeiros terminem para lançar os ultimos dois
        supervisor.startDriver(d3).done();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        supervisor.startDriver(d4).done();

        try {
            d3.join();
            d4.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        //Espera que os civis acabem
        for(Civilian civilian : civilianList){
           try {
               civilian.join();
           } catch (InterruptedException e) {
               e.printStackTrace();
           }
       }

       //Termina as threads ainda activas
       out.println("Done");

        supervisor.terminate();
        board.terminate();
    }
}
